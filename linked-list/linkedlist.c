#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#define NR_ITEMS (10u)

typedef struct node_t{
    uint8_t value;
    struct node_t* next;
}node_t;

void push(node_t** headRef, uint8_t value);
void print(node_t** node);

int main(void) {

    uint16_t seed = 1337;
    srand(seed);

    uint8_t values[NR_ITEMS] = { 0 };
    for(int i = 0; i < NR_ITEMS; i++){
        values[i] = rand() % 256;
    }

    /* insert these values into a linked list */

    node_t* head = (node_t*) malloc(sizeof(node_t));
    head->value = 5;
    head->next = NULL;

    for(int i = 0; i < NR_ITEMS; i++){
        push(&head, values[i]);
    }
 
    /* print the linked list */

    print(&head);
    
    return 0;
}

/* Take pointer to HEAD and insert value as new HEAD.*/
void push(node_t** headRef, uint8_t value){
    node_t* newNode = (node_t*) malloc(sizeof(node_t));
    newNode->value = value;
    newNode->next = (*headRef);
    (*headRef) = newNode;
}

void print(node_t** headRef){
    node_t* dummy = (*headRef);
    while(dummy->next != NULL){
        printf("%d ", dummy->value);
        dummy = dummy->next;
    }
    printf("\n");
}
